# Project

This project was created using Asp.Net Core 3.1, Entity Framework Core 5.0.5 and Angular 11.2.9

# Purpose
Create a web application with the list of the employees by using angular 2. If you are more friendly with some other framework, please use it.
UseCases:
we need a list of all employees (nice to have: with search)
we can hire a new employee and we need to register these properties:
name
surname
work position (we can choose from an enum, which is available on API method GET, on URL: http://ibillboard.com/api/positions)
date of birth
we can change name of employee
employee can change work position
employee can leave the company
Tips:
UI must be user-friendly and easy to control (try to to use it like user)
create unit tests, if you know how
think about clean code and reusing code
someone will try to run your app, think on this

## Database
The project is configured to work in a local SQL Server (localdb) instance, but you can change the connection string in Satrtup.cs file to point to another instance, even remote.
EF core doesn't support automatic migrations.So you have to do the database migration manually following the instructions bellow.

## Development Environment

* Open the solution
* Click on Tools\Nuget Package Manager\Package Manager Console
* Select "Model" in Default project dropdown list (in top bar)
* Type "EntityFrameworkCore\Update-Database" (without quotation marks (")) and press Enter
* Press F5 keyboard key to launch the server (this will also run "npm install" and "ng serve" commands in background)

## Troubleshooting

If you have any problems creating the database, you can restore the backup copy (Employees.bak) located under root path and swith to the commented connection sring:

//services.AddDbContext<AppDbContext>(
//        options => options.UseSqlServer("name=ConnectionStrings:DefaultConnection"));


