﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.DTOs
{
    public class DateStructDto
    {
        public DateStructDto() { }
        public DateStructDto(DateTime d)
        {
            this.Year = d.Year;
            this.Month = d.Month;
            this.Day = d.Day;
            this.Hour = d.Hour;
            this.Minute = d.Minute;
            this.Second = d.Second;
        }

        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }
        public int Second { get; set; }

        public DateTime ToDate()
        {
            return new DateTime(Year, Month, Day, Hour, Minute, Second);
        }
    }
}
