﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.DTOs
{
    public class EmployeeDto 
    {
        public EmployeeDto() { }
        public EmployeeDto(Employee c)
        {
            Id = c.Id;
            Name = c.Name;
            Surname = c.Surname;
            WorkPosition = c.WorkPosition;
            DateOfBirth = new DateStructDto(c.DateOfBirth);
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string WorkPosition { get; set; }
        public DateStructDto DateOfBirth { get; set; }
    }
}
