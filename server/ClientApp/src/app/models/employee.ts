import { DateStruct } from "./dateStruct";
import { UserRoleEnum } from "./user-role-enum";

export interface Employee {
  id: number;
  name?: string;
  role?: UserRoleEnum
  surname?: string;
  workPosition?: string;
  dateOfBirth?: DateStruct;
}
