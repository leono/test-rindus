/**
 * A simple way to send and receive dates from/to backend, avoiding timezone conversions.
 */
export class DateStruct {
  year: number;
  month: number;
  day: number;
  hour: number;
  minute: number;
  second: number;

  static toDate(d: DateStruct) : Date {
    return new Date(d.year, d.month-1, d.day, d.hour, d.minute, d.second);
  }
}
