import { UserRoleEnum } from "./user-role-enum";

export interface JwtToken {
  userId: number;
  userName: string;
  token: string;
  result: boolean;
  role: UserRoleEnum;
}
