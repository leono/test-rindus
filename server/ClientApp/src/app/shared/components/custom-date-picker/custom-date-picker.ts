import { Component, Injectable, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { NgbDatepickerI18n, NgbCalendar, NgbDateAdapter, NgbDateParserFormatter, NgbDateStruct, NgbDate, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR, NG_VALIDATORS } from '@angular/forms';

const I18N_VALUES = {
    en: {
        weekdays: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
        months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    },
    fr: {
        weekdays: ['Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa', 'Di'],
        months: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Juin', 'Juil', 'Aou', 'Sep', 'Oct', 'Nov', 'Déc'],
    },
    es: {
        weekdays: ['Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom'],
        months: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        //     firstDayOfWeek: 1,
        //     closeText: 'Cerrar',
        //     prevText: 'Anterior',
        //     nextText: 'Siguiente',
        //     monthNames: ['Enero','Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        //     monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
        //     dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
        //     dayNamesShort: ['Dom','Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
        //     dayNamesMin: ['D','L','M','X','J','V','S'],
        //     weekHeader: 'Semana',
        //     firstDay: 1,
        //     isRTL: false,
        //     showMonthAfterYear: false,
        //     yearSuffix: '',
        //     timeOnlyTitle: 'Sólo hora',
        //     timeText: 'Tiempo',
        //     hourText: 'Hora',
        //     minuteText: 'Minuto',
        //     secondText: 'Segundo',
        //     currentText: 'Fecha actual',
        //     ampm: false,
        //     month: 'Mes',
        //     week: 'Semana',
        //     day: 'Día',
        //     allDayText : 'Todo el día'
    }
};


/**
 * This Service handles how the date is represented in scripts i.e. ngModel.
 */
@Injectable()
export class CustomAdapter extends NgbDateAdapter<string> {

    readonly DELIMITER = '-';

    fromModel(value: any | null): NgbDateStruct | null {
        if (value instanceof NgbDate || (value && value['day'])) {
            return value;
        }

        if (typeof(value) === "string") {
            let date = value.split(this.DELIMITER);
            return {
                day: parseInt(date[0], 10),
                month: parseInt(date[1], 10),
                year: parseInt(date[2], 10)
            };
        }
        return null;
    }

    toModel(date: NgbDateStruct | null): string | null {
        return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : null;
    }
}

/**
 * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
 */
@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {

    readonly DELIMITER = '/';

    parse(value: string): NgbDateStruct | null {
        if (value) {
            let date = value.split(this.DELIMITER);
            return {
                day: parseInt(date[0], 10),
                month: parseInt(date[1], 10),
                year: parseInt(date[2], 10)
            };
        }
        return null;
    }

    format(date: NgbDateStruct | null): string {
        return date ? date.day + this.DELIMITER + date.month + this.DELIMITER + date.year : '';
    }
}

// Define a service holding the language. You probably already have one if your app is i18ned.
@Injectable()
export class I18n {
    language = 'es';
}

// Define custom service providing the months and weekdays translations
@Injectable({
    providedIn: 'root'
})
export class CustomDatepickerServiceI18n extends NgbDatepickerI18n {
    getDayAriaLabel(date: NgbDateStruct): string {
        return `${date.day}-${date.month}-${date.year}`;
    }

    constructor(private _i18n: I18n) {
        super();
    }

    getWeekdayShortName(weekday: number): string {
        return I18N_VALUES[this._i18n.language].weekdays[weekday - 1];
    }
    getMonthShortName(month: number): string {
        return I18N_VALUES[this._i18n.language].months[month - 1];
    }
    getMonthFullName(month: number): string {
        return this.getMonthShortName(month);
    }
}

@Component({
    selector: 'app-custom-date-picker',
    templateUrl: './custom-date-picker.html',
    providers: [I18n,
        { provide: NgbDatepickerI18n, useClass: CustomDatepickerServiceI18n },// define custom NgbDatepickerI18n provider
        { provide: NgbDateAdapter, useClass: CustomAdapter },
        { provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter },
        { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => CustomDatepicker), multi: true },
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => CustomDatepicker), multi: true }
    ]
})
export class CustomDatepicker implements ControlValueAccessor {
    @Input() value: NgbDateStruct;
    @Input() maxDate: NgbDateStruct;
    @Input() minDate: NgbDateStruct;
    @Input() placeHolder: string;
    @Input() readonly: boolean;
    @Output() onDateSelected = new EventEmitter<NgbDateStruct>();

    private onTouched = () => { };
    propagateChange: any = () => { };
    validateFn: any = () => { };
    today = this.ngbCalendar.getToday();

    constructor(
        private _i18n: I18n,
        private ngbCalendar: NgbCalendar,
        private dateAdapter: NgbDateAdapter<string>) { }

    set language(language: string) {
        this._i18n.language = language;
    }

    get language() {
        return this._i18n.language;
    }

    public writeValue(value: any): void {
        if (value) {
            this.setValue(value);
        }
    }

    private setValue(value: NgbDateStruct): void {
        this.value = value;
        this.onTouched();
        this.propagateChange(value);
        this.onDateSelected.emit(value);
    }

    public registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    public registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    public validate(control: FormControl) {
        return this.validateFn(control, this.value);
    }

    onDateSelect(value: any) {
        this.setValue(this.dateAdapter.fromModel(value));
    }

    displayCalendar(dt: NgbInputDatepicker): void {
        setTimeout(() => {
            dt.open();
        }, 1);
    }
}

