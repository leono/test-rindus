import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomDatepicker } from 'src/app/shared/components/custom-date-picker/custom-date-picker';


@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, NgbModule],
    declarations: [CustomDatepicker],
    exports: [CustomDatepicker]
})
export class CustomDatepickerModule {}
