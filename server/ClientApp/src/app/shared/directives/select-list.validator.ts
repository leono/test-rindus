import { Directive, Input, NgModule } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';

export interface SelectListValidatorConfiguration {
    dataSource?: any[];
    valueName?: string;
}

@Directive({
    selector: '[appSelectListValidator]',
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: SelectListValidator,
            multi: true
        }
    ]
})
export class SelectListValidator implements Validator {
    @Input() appSelectListValidator: SelectListValidatorConfiguration;

    static SelectListValidation(config: SelectListValidatorConfiguration): ValidatorFn {
        return (control: AbstractControl): ValidationErrors => {
            return SelectListValidator.validateList(control, config);
        };
    }
    private static validateList(control: AbstractControl, config: SelectListValidatorConfiguration): { [key: string]: any } | null {
        const res = { defaultselected: true };

        if (control.value !== undefined && config && config.dataSource
            && config.dataSource.length > 0) {

            if (config.valueName && config.valueName.length > 0) {
                return config.dataSource.some(obj => obj[config.valueName] === control.value) ? null : res;
            } else {
                return config.dataSource.some(obj => obj === control.value) ? null : res;
            }
        }

        return res;
    }

    validate(control: AbstractControl): { [key: string]: any } | null {
        return SelectListValidator.validateList(control, this.appSelectListValidator);
    }

}

@NgModule({
    declarations: [SelectListValidator],
    exports: [SelectListValidator]
})
export class SelectListValidatorModule { }

