import { HttpClient, HttpParams } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";
import { JwtToken } from "src/app/models/jwt-token";
import { environment } from "src/environments/environment";
import { BaseService } from "./base.service";
import { HttpService } from "./http.service";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService extends BaseService {
  static TOKEN_KEY: string = 'token';
  private currentUserSubject: BehaviorSubject<JwtToken>;
  public currentUser: Observable<JwtToken>;

  constructor(
    private httpService: HttpService,
    private router: Router) {
    super();

    this.currentUserSubject = new BehaviorSubject<JwtToken>(JSON.parse(localStorage.getItem(AuthenticationService.TOKEN_KEY)));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public login(userId: string): Observable<boolean> {
    const params = new HttpParams()
      // .set('grant_type', 'password')
      .set('userId', userId);
    // .set('username', userName);
    // .set('password', password);

    const response = this.httpService.postForm(`${environment.baseUrl}employee/login`, params.toString());

    const callBack = response.pipe(map((data: JwtToken) => {
      if (data && data.result === true) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        // const token = (<AccessToken>data).access_token;
        localStorage.setItem(AuthenticationService.TOKEN_KEY, JSON.stringify(data));
        this.currentUserSubject.next(data);
      }

      return true;
    }));

    return this.wrapObservable(callBack);
  }

  public logout(redirectToLogin = true) {
    // remove user from local storage to log user out
    localStorage.removeItem(AuthenticationService.TOKEN_KEY);
    this.currentUserSubject.next(null);

    if (redirectToLogin) {
      this.router.navigate(['/login']);
    } else {
      this.router.navigate(['/']);
    }
  }

  public isLoggedInUser(): boolean {
    return this.currentUserSubject.value && this.currentUserSubject.value.token.length > 0;
  }

  getToken(): string {
    return localStorage.getItem(AuthenticationService.TOKEN_KEY);
  }

  public getLoggedInUser(): JwtToken {
    return this.currentUserSubject.value;
  }
}
