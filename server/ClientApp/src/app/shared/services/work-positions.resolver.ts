import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import { map, tap } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { HttpService } from "./http.service";

@Injectable({
	providedIn: 'root'
})
export class WorkPositionsService {
  workPositions: string[];

	constructor(private http: HttpService) { }

	get(): Observable<string[]> {

		//call without headers to avoid CORS problems
    return this.http.get('https://www.ibillboard.com/api/positions', null, {})
    // we could call our api as well to hide or avoid CORS problems
    // return this.http.get(`${environment.baseUrl}employee/workPositions`)
      .pipe(
        map((p: any) => p.positions),
        tap(data => {
          // save a local copy
          this.workPositions = data;
      }));
	}
}

@Injectable({
	providedIn: 'root'
})
export class WorkPositionsResolver implements Resolve<string[]> {

  constructor(private workPositionsService: WorkPositionsService) { }

  resolve(route: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): Observable<string[]> {
    return this.workPositionsService.get();
  }
}
