import { catchError } from 'rxjs/operators';
import { Observable, throwError } from "rxjs";

export class BaseService {
  constructor() {

  }

  wrapObservable(callBack: Observable<any>) {
      return callBack.pipe(
          catchError(err => {
              console.log('Handling error locally at wrapObservable()', err);
              return throwError(err);
          }));
  }

}
