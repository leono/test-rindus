import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { Employee as Employee } from "../../models/employee";
import { HttpService } from "./http.service";

@Injectable({
	providedIn: 'root'
})
export class EmpoloyeeService {
  employeeList: Employee[];

	constructor(private http: HttpService) { }

	getById(id: string): Observable<Employee> {
		return this.http.get(`${environment.baseUrl}employee/${id}`);
	}

	get(): Observable<Employee[]> {
		return this.http.get(environment.baseUrl + 'employee')
      .pipe(tap(data => {
        // save a local copy
        this.employeeList = data;
      }));
	}

	post(employee: Employee): Observable<boolean> {
		return this.http.postJSon(environment.baseUrl + 'employee', employee);
	}

	put(employee: Employee): Observable<boolean> {
		return this.http.put(environment.baseUrl + 'employee', employee);
	}

	leave(): Observable<any> {
		return this.http.postForm(environment.baseUrl + 'employee/leave', null);
	}
}
