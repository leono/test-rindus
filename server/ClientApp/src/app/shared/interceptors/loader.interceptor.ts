import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoadingService } from '../services/loading.service';


@Injectable({
    providedIn: 'root'
})
/**
 * This class is for loading indicators management. It intercepts all requests and set the status of the global loading variable which
 * indicates if there are pending requests
 */
export class LoaderInterceptor implements HttpInterceptor {

    /**
     * Use this for operations in background which don't need to be intercepted by this class
     * Usage:
     *
         const customHeaders = {
         [EXCLUDE_REQUEST]: EXCLUDE_REQUEST
        };

        this.apiService.put(`api/reception/updatetourtsw`, modelData, customHeaders);
    *
    */
    private static readonly EXCLUDE_REQUEST = 'EXCLUDE_REQUEST';
    static readonly EXCLUDE_REQUEST_HEADER = {[LoaderInterceptor.EXCLUDE_REQUEST]: 'true'};

    // Counter which prevents hiding loader indicator when multiple http requests are being processed and not all have finished
    private httpCount = 0;

    constructor(private loadingService: LoadingService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const excludeRequest = request.headers.get(LoaderInterceptor.EXCLUDE_REQUEST) === 'true';

        if (!excludeRequest) {
            this.showLoader();
        }

        return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse && !excludeRequest) {
                this.hideLoader();
            }
        }, () => {
            if (!excludeRequest) {
                this.hideLoader();
            }
        }));
    }
    private hideLoader() {
        this.httpCount--;
        if (this.httpCount === 0) {
            this.loadingService.loading = false;
        }
    }
    private showLoader() {
        this.loadingService.loading = true;
        this.httpCount++;
    }

}
