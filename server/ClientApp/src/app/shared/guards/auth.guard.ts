import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map } from 'rxjs/operators';
import { UserRoleEnum } from '../../models/user-role-enum';
import { AuthenticationService } from '../services/authentication.service';
import { EmpoloyeeService } from '../services/employee.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private empoloyeeService: EmpoloyeeService) { }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    //if employee list is already loaded
    if (this.empoloyeeService.employeeList?.length === 0) {
      //there's no users, so it allows the creation of the administrator
      return true;
    } else {
      return this.empoloyeeService.get().pipe(map(data => {
        if (data.length === 0) {
          //there's no users, so it allows the creation of the administrator
          return true;
        } else {
          return this.checkLoggedInUser(route, state);
        }
      }));
    }
  }

  checkLoggedInUser(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.authenticationService.isLoggedInUser()) {

      // let urlPath = state.url.replace('%20', ' ').split('/');
      var user = this.authenticationService.getLoggedInUser();
      let roles = route.data.roles as Array<UserRoleEnum>;

      //this checks the user permission role coming form canActivate --> [AuthGuard], data: {roles: [UserRoleEnum.Admin, UserRoleEnum.Manager]}
      return !roles || roles.filter(role => role === user.role).length > 0;
    } else {
      // not logged in so redirect to login page with the return url
      // replace %20 with a white space to avoid converting % to %25 and geting %2520
      let url = state.url;
      url = url.replace('%20', ' ');

      const path = 'login' + (route.data.routeParam ? route.data.routeParam : '');

      this.router.navigate([path], { queryParams: { returnUrl: url } });
      return false;
    }
  }
}
