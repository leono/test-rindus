import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Employee } from 'src/app/models/employee';
import { UserRoleEnum } from 'src/app/models/user-role-enum';
import { FrmBase } from 'src/app/tools/frmBase';

/**
 * Using enums and referencing this in all the code (even html files) will make the code much more cleaner, easier to understand and maintain and will give you
 * a full sight of what you need to do to change or add new functionalities to existing code.
 */
export enum EmployeeFormEnum {
  ID = 'id',
  NAME = 'name',
  SURNAME = 'surname',
  WORK_POSITION = 'workPosition',
  DATE_BIRTH = 'dateOfBirth',
}

/**
 * This class it's used to define the form fields and the needed logic such as setting or removing validators under certain scenarios or even do some checks
 */
export class EditEmployeeForm extends FrmBase<EmployeeFormEnum, Employee> {

  constructor(
    protected formBuilder: FormBuilder,
    private data: Employee,
    userRole: UserRoleEnum,
    editMode: boolean) {

    super(formBuilder);

    this.formFieldsDefinitionList = [
      { fieldName: EmployeeFormEnum.ID, validators: [] },
      { fieldName: EmployeeFormEnum.NAME, validators: [Validators.required] },
      { fieldName: EmployeeFormEnum.SURNAME, validators: [Validators.required] },
      { fieldName: EmployeeFormEnum.WORK_POSITION, validators: [], disabled: editMode && userRole === UserRoleEnum.Admin},
      { fieldName: EmployeeFormEnum.DATE_BIRTH, validators: [Validators.required] },
    ];

    this.init(data);
  }

  resetCustom() {
    this.reset();
    this.setDefaultDate();
  }

  public setDefaultDate(value = undefined) {
    const today = new Date();
    if (value === undefined) {
      value = {
        day: today.getDate(),
        month: today.getMonth() + 1,
        year: today.getFullYear()
      };
    }
    this.getControl(EmployeeFormEnum.DATE_BIRTH).setValue(value);
  }

  /**
   * Overriding base class method
   * This is just a demostration how to override methods from base class and how to create new instances with the spread operator (...)
   *
   * @returns Employee
   */
  public mapToModel(): Employee {
    const data : Employee =
    {
      ...super.mapToModel(),
      role: this.data.role //this is already being done on backend side, but it demostrates how to use the spread operator to create new data structures form other data
    };

    return data;
  }

}
