import 'src/app/tools/date-extension';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subscription } from 'rxjs';
import { DateStruct } from 'src/app/models/dateStruct';
import { Employee } from 'src/app/models/employee';
import { EmpoloyeeService } from 'src/app/shared/services/employee.service';
import { BaseComponent } from '../base.component';
import { EditEmployeeForm, EmployeeFormEnum } from './edit-employee.form';
import { WorkPositionsService } from 'src/app/shared/services/work-positions.resolver';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { UserRoleEnum } from 'src/app/models/user-role-enum';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent extends BaseComponent implements OnInit, OnDestroy {
  editMode = false;
  frmManager: EditEmployeeForm;
  employeeFormEnum = EmployeeFormEnum; //this is needed to use enums in html template
  mindate = new Date(1975, 1, 1);

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private employeeService: EmpoloyeeService,
    public workPositionsService: WorkPositionsService,
    public authenticationService: AuthenticationService) {
      super();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.route.params.subscribe(
        params => {
          if (params['id']) {
            this.subscriptions.add(
              this.employeeService.getById(params['id']).subscribe(result => {
                this.editMode = true;
                this.createForm(result);
              },
                error => console.error(error))
            );
          } else {
            this.createForm();
          }
        })
    );
  }

  createForm(employee: Employee = null) {
    if (!employee) {
      employee = { id: 0, workPosition: null };
    }

    this.frmManager = new EditEmployeeForm(this.fb, employee, this.authenticationService.getLoggedInUser()?.role,
      this.editMode);
  }

  onSubmit() {
    this.frmManager.submitted = true; //enable display errors
    if (this.frmManager.form.valid) {
      let subscriber: Observable<boolean>;
      const data : Employee = this.frmManager.mapToModel();

      if (this.editMode) {
        subscriber = this.employeeService.put(data);
      } else {
        subscriber = this.employeeService.post(data);
      }

      this.subscriptions.add(
        subscriber.subscribe(result => {
          if (result === true) {
            this.router.navigate(['/']);
          }
        }, error => console.error(error))
      );
    }
  }

  onDateSelected(date: NgbDateStruct): void {
  }

  canEdit(fieldName: EmployeeFormEnum) : boolean {
    const allowedAdmin = this.authenticationService.getLoggedInUser()?.role === UserRoleEnum.Admin
      && fieldName === EmployeeFormEnum.NAME;

    /*
    NOTE: as WorkPosition is rendered with a select list and According to HTML specs, the select tag in HTML
    doesn't have a readonly attribute, only a disabled attribute.
    But disabled attribute should be managed by Reactive Forms, avoiding disabledAttrWarning
    */
    // const allowedUser = this.authenticationService.getLoggedInUser().role === UserRoleEnum.User
    //   && fieldName === EmployeeFormEnum.WORK_POSITION;

    return this.editMode && (!allowedAdmin /* || !allowedUser */);
  }

  canLeaveCompany() {
    return this.editMode && this.authenticationService.getLoggedInUser()?.role === UserRoleEnum.User;
  }

  onLeaveCompany() {
    if(confirm("Are you sure to leave the company? This action can't be undone.")) {
      this.employeeService.leave().subscribe(result => {
        if (result === true) {
          this.authenticationService.logout();
          this.router.navigate(['/']);
        }
      }, error => console.log(error));
    }
  }

}
