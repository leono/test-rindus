import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BaseComponent } from '../base.component';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { LoginForm, LoginFormFieldsEnum } from './login.form';
import { LoadingService } from 'src/app/shared/services/loading.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent extends BaseComponent implements OnInit, OnDestroy {
    frmManager: LoginForm;
    returnUrl: string;
    loginFail: boolean;
    loginFormFieldsEnum = LoginFormFieldsEnum; //this is needed to use enums in html template

    constructor(
        public router: Router,
        private fb: FormBuilder,
        private route: ActivatedRoute,
        public authenticationService: AuthenticationService,
        public loadingService: LoadingService) {
            super();
            // get return url from route parameters or default to '/'
            this.returnUrl = (this.route.snapshot.queryParams['returnUrl'] || '/');
        }

    ngOnInit() {
        this.frmManager = new LoginForm(this.fb);
    }

    ngOnDestroy(): void {
        this.subscriptions.unsubscribe();
    }

    onLoggedin() {
        this.loginFail = false;
        this.frmManager.submitted = true;

        if (this.frmManager.form.valid) {
            this.subscriptions.add(
                this.authenticationService.login(this.frmManager.getControl(LoginFormFieldsEnum.USER_ID).value).subscribe(
                    loginResult => {
                        if (loginResult === true) {
                            if (this.returnUrl && this.returnUrl.length > 1 ) {
                                this.router.navigate([this.returnUrl]).then(onfulFilled => {
                                  this.loginFail = onfulFilled === false;
                                });
                            } else {
                                this.router.navigate(['/']);
                            }
                        } else {
                            this.loginFail = true;
                        }
                    },
                    error => {
                        this.loginFail = true;
                    },
                )
            );
        }
    }
}
