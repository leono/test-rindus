import { FormBuilder, Validators } from "@angular/forms";
import { FrmBase } from "src/app/tools/frmBase";

export enum LoginFormFieldsEnum {
  USER_ID = 'userId'
}

export class LoginForm extends FrmBase<LoginFormFieldsEnum, any> {

  constructor(protected formBuilder: FormBuilder) {

    super(formBuilder);

    this.formFieldsDefinitionList = [
      { fieldName: LoginFormFieldsEnum.USER_ID, validators: [Validators.required] },
    ];

    this.init();
  }

}
