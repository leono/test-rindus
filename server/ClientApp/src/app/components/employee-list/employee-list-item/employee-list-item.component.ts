import { Component, Input } from '@angular/core';
import { Employee } from 'src/app/models/employee';
import { DateStruct } from 'src/app/models/dateStruct';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { JwtToken } from 'src/app/models/jwt-token';
import { UserRoleEnum } from 'src/app/models/user-role-enum';

@Component({
  selector: 'tr[app-employee-list-item]',
  templateUrl: './employee-list-item.component.html',
  styleUrls: ['./employee-list-item.component.css']
})
export class EmployeeListItemComponent {
  @Input() employee: Employee;
  dateStructHelper = DateStruct;
  user: JwtToken;
  userRoleEnum = UserRoleEnum;

  constructor(private authenticationService: AuthenticationService) {
    this.user = authenticationService.getLoggedInUser();
  }
}

