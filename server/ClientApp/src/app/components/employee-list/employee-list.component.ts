import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from 'src/app/models/employee';
import { EmpoloyeeService } from 'src/app/shared/services/employee.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  // Using async pipes we take advantage of avoiding forget to unsubscribe from observables and also simplify needed code to get data from backend
  employees$ = this.employeeService.get();

  // using subscriptions to fill this variable need also to implement OnDestroy() interface to ensure we unsubscribe from Observable subscriptions
  // public employees: Employee[];

  constructor(
    private employeeService: EmpoloyeeService,
    public loadingService: LoadingService,
    private authenticationService: AuthenticationService) {
  }

  ngOnInit(): void {
    this.loadingService.loading = true; //little trick to avoid the ugly ExpressionChangedAfterItHasBeenCheckedError error

    //refresh the employee list when user identity changes
    this.authenticationService.currentUser.subscribe(u => {
      this.employees$ = this.employeeService.get();
    });
    // this.employeeService.get().subscribe(result => {
    //   this.employees = result;
    // }, error => console.error(error));
  }
}

