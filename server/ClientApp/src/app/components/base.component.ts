import { Subscription } from 'rxjs';
import { Injector } from '@angular/core';

export class ServiceLocator {
    static injector: Injector;
}

export class BaseComponent {
    moduleKey: string;
    protected subscriptions: Subscription = new Subscription();

    constructor() { }

}
