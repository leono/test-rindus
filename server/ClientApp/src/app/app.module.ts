import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { HomeComponent } from './components/home/home.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { EditEmployeeComponent } from './components/edit-employee/edit-employee.component';
import { AuthGuard } from './shared/guards/auth.guard';
import { LoaderInterceptor } from './shared/interceptors/loader.interceptor';
import { EmployeeListItemComponent } from './components/employee-list/employee-list-item/employee-list-item.component';
import { CustomDatepickerModule } from './shared/components/custom-date-picker/custom-date-picker.module';
import { WorkPositionsResolver } from './shared/services/work-positions.resolver';
import { SelectListValidatorModule } from './shared/directives/select-list.validator';
import { JwtInterceptor } from './shared/interceptors/jwt.interceptor';
import { UserRoleEnum } from './models/user-role-enum';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full', resolve: [WorkPositionsResolver] },
  { path: 'login', loadChildren: () => import('./components/login/login.module').then((m) => m.LoginModule) },
  {
    path: 'new',
    component: EditEmployeeComponent,
    canActivate: [AuthGuard],
    resolve: [WorkPositionsResolver],
    data: { roles: [UserRoleEnum.Admin] } //add specific role as route parameter
  },
  {
    path: 'edit/:id',
    component: EditEmployeeComponent,
    canActivate: [AuthGuard],
    resolve: [WorkPositionsResolver]
  },
];

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    EmployeeListComponent,
    EmployeeListItemComponent,
    EditEmployeeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    CustomDatepickerModule,
    SelectListValidatorModule
  ],
  providers: [ AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
