
interface Date {
  addInterval(this: Date, addTime): Date;
  dateCompare(this: Date, dateCompared: Date, compareTime: boolean): number;
}

function addInterval(this: Date, addTime: { hours: number, minutes: number, seconds: number }) : Date {
  let generatedTime = this.getTime();
  if (addTime.seconds) generatedTime += 1000 * addTime.seconds; //check for additional seconds
  if (addTime.minutes) generatedTime += 1000 * 60 * addTime.minutes;//check for additional minutes
  if (addTime.hours) generatedTime += 1000 * 60 * 60 * addTime.hours;//check for additional hours
  return new Date(generatedTime);
}

function dateCompare(this: Date, dateCompared: Date, compareTime: boolean): number {
  let res = 0;
  let tmpDate1: Date;
  let tmpDate2: Date;

  if (compareTime) {
      tmpDate1 = new Date(this.getFullYear(), this.getMonth(), this.getDate(), this.getHours(), this.getMinutes(), this.getSeconds());
      tmpDate2 = new Date(dateCompared.getFullYear(), dateCompared.getMonth(), dateCompared.getDate(), dateCompared.getHours(), dateCompared.getMinutes(), dateCompared.getSeconds());
  } else {
      tmpDate1 = new Date(this.getFullYear(), this.getMonth(), this.getDate());
      tmpDate2 = new Date(dateCompared.getFullYear(), dateCompared.getMonth(), dateCompared.getDate());
  }

  if (tmpDate1.getTime() === tmpDate2.getTime()) {
      res = 0
  } else if (tmpDate1.getTime() > tmpDate2.getTime()) {
      res = 1
  } else if (tmpDate1.getTime() < tmpDate2.getTime()) {
      res = -1
  }

  return res;
}

/**
* USAGE:
  let futureDate = new Date().addInterval({
      hours: 16, //Adding one hour
      minutes: 45, //Adding fourty five minutes
      seconds: 0 //Adding 0 seconds return to not adding any second so  we can remove it.
  });
*/
Date.prototype.addInterval = addInterval;

Date.prototype.dateCompare = dateCompare;
