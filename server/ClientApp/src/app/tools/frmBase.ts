import './reactive-forms-extension'
import { FormControl, FormGroup, FormBuilder, AbstractControl, ValidatorFn, Validators } from '@angular/forms';

export interface FormFieldsDefinition<T> {
    fieldName?: T;
    fieldGroup?: FormFieldsDefinitionGroup<T>;
    validators?: ValidatorFn[];
    disabled?: boolean;
    noAutoRefresh?: boolean;
}

export interface FormFieldsDefinitionGroup<T> {
    fieldName?: string;
    group: FormFieldsDefinition<T>[];
    validators?: ValidatorFn[];
    disabled?: boolean;
    noAutoRefresh?: boolean;
}


/**
 * @author: Leonardo Onieva (22/09/2020)
 * @description: The intention of this class is to do a basic but standarized way to manage reactive forms.
 *
 * <TControlNames> is typically an Enum type (string) which contains the list of the form field names
 * <TReturnType> is the type which will be returned by mapToModel() method
 */
export class FrmBase<TControlNames, TReturnType> {
    // tslint:disable-next-line: max-line-length
    public static emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    public form: FormGroup;

    /**
     * Form fields definition list.
     * Add or remove the needed form fields in the derived class initialization
     * NOTE: Field names must match to webapi model
     */
    public formFieldsDefinitionList: FormFieldsDefinition<TControlNames>[];
    submitted = false;

    /**
     * Set a form control required or not
     *  @dependencies: import '@/reactive-forms-extension';
     *
     * @param control: name of the form control
     * @param value: true/false
     */
    public static setRequired(control: AbstractControl, value: boolean) {
        control.setRequired(value);
    }

    /**
     * Set a form control enabled or not
     *  @dependencies: import '@/reactive-forms-extension';
     *
     * @param control: name of the form control
     * @param value: true/false
     */
    public static setEnabled(control: AbstractControl, value: boolean) {
        control.setEnabled(value);
    }

    constructor(
        protected formBuilder: FormBuilder) {
    }

    /**
     * Set a form control required or not
     *  @dependencies: import '@/reactive-forms-extension';
     *
     * @param controlName: name of the form control
     * @param value: true/false
     */
    public setRequired(controlName: TControlNames, value: boolean) {
        FrmBase.setRequired(this.getControl(controlName), value);
    }

    /**
     * Set a form control enabled or not
     *  @dependencies: import '@/reactive-forms-extension';
     *
     * @param controlName: name of the form control
     * @param value: true/false
     */
    public setEnabled(controlName: TControlNames, value: boolean) {
        this.getControl(controlName).setEnabled(value);
    }


    public init(data: any = null) {
        if (!this.form) {
            this.create(data);
        } else {
            this.update(data);
        }
    }

    private create(data: any) {
        this.form = this.formBuilder.group({});
        let fieldNames = this.formFieldsDefinitionList ? this.formFieldsDefinitionList :
            Object.keys(data).map(elem => { return { fieldName: elem, validators: [Validators.required] }; });

        fieldNames.forEach(element => {
            if (element.fieldName) {
                this.addControl(this.form, element, data);

            } else if (element.fieldGroup) {
                const gr = this.formBuilder.group({ }, {validator: element.fieldGroup.validators});
                element.fieldGroup.group.forEach(grElement => {
                    this.addControl(gr, grElement, data);
                });
                this.form.addControl(element.fieldGroup.fieldName, gr);
            }
        });
    }

    addControl(form: FormGroup, element: FormFieldsDefinition<TControlNames>, data: any) {
        form.addControl(
            element.fieldName.toString(),
            new FormControl(
                {
                    value: data ? data[element.fieldName] : null,
                    disabled: element.disabled
                },
                element.validators)
        );
    }

    protected update(data: any) {
        this.formFieldsDefinitionList.forEach(element => {
            if (!element.noAutoRefresh) {
                this.getControl(element.fieldName).setValue(data ? data[element.fieldName] : null);

                FrmBase.setEnabled(this.getControl(element.fieldName), !element.disabled);

                this.getControl(element.fieldName).setValidators(element.validators);
            }
        });
    }

    /**
     * Returns a form field control
     * @param controlName: string which matches with a form field control
     */
    public getControl(controlName: TControlNames): FormControl {
        return this.form.get(controlName as any) as FormControl;
    }


    /**
     * find invalid controls passing form html ex: {{ errSvc.findInvalidControls(articleForm.root.controls) }}
     * Usage:
     *   let tmp = this.frm.findInvalidControls(this.drugForm.root.controls);
     *   console.log(tmp);
     *
     * @param controls: form controls collection to check out.
     * @param invalidList: parameter which is filled recursively when form fields contain nested controls
     */
    public findInvalidControls(controls: { [key: string]: AbstractControl; }, invalidList = []) {

        for (const name in controls) {
            if (controls[name] && controls[name].invalid) {
                const obj = controls[name] as FormGroup;
                if (obj && obj.controls) {
                    this.findInvalidControls(obj.controls, invalidList);
                } else {
                    invalidList.push({ controlName: name, field: controls[name] });
                }
            }
        }
        if (invalidList.length > 0) { console.log(invalidList); }

        return invalidList;
    }

    public mapToModel(): TReturnType {
        return {...this.form.getRawValue()} as TReturnType;
    }

    public isDisplayError(controlName: TControlNames, errorCodes: string[] = null, formGroup: AbstractControl = this.form): boolean {
        let displayError = false;
        const formControl = formGroup.get(controlName.toString());

        if (errorCodes != null && errorCodes.length > 0 && formControl?.errors) {
            errorCodes.forEach(errorCode => {
                if (formControl.errors[errorCode]) {
                    displayError = true;
                }
            });
        } else {
            displayError = formControl?.invalid;
        }
        return displayError && this.submitted;
    }

    reset() {
        this.submitted = false;
        this.form.reset();
    }
}
