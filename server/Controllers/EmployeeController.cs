﻿using Business.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Model.Database;
using Model.Models;
using Newtonsoft.Json;
using server.DTOs;
using server.Helpers;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [JwtAuthorize]
    public class EmployeeController : ControllerBase
    {
        private readonly ILogger<EmployeeController> _logger;
        private readonly IUserService _userService;
        private readonly IEmployeeService _employeeService;

        public EmployeeController(ILogger<EmployeeController> logger, IUserService userService, 
            IEmployeeService employeeService)
        {
            _logger = logger;
            _userService = userService;
            _employeeService = employeeService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IEnumerable<EmployeeDto>> Get()
        {
            return (await _employeeService.GetList())
                .Select(c => new EmployeeDto(c));
        }

        [HttpGet("workPositions")]
        public async Task<IActionResult> GetWorkPositions()
        {
            WorkPositionsDto workPositions = null;
            string url = @"https://www.ibillboard.com/api/positions";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)(await request.GetResponseAsync()))
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                workPositions = JsonConvert.DeserializeObject<WorkPositionsDto>(reader.ReadToEnd());
            }

            return Ok(workPositions);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var employee = await _employeeService.GetById(id);
            if (employee != null)
                return Ok(new EmployeeDto(employee));
            else
                return NotFound();

        }

        [HttpPost]
        public async Task<bool> Post(EmployeeDto data)
        {
            Employee employee = new Employee 
            {
                Name = data.Name,
                DateOfBirth = data.DateOfBirth.ToDate(),
                Role = Employee.UserRolesEnum.User,
                Surname = data.Surname,
                WorkPosition = data.WorkPosition
            };
            return await _employeeService.NewEmployee(employee);
        }

        [HttpPost("{login}")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromForm]LoginDto data)
        {
            var user = await _employeeService.GetById(data.UserId);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });
            else
                return Ok(_userService.Authenticate(user));
        }

        [HttpPut]
        public async Task<IActionResult> Put(EmployeeDto data)
        {
            Employee user = HttpContext.Items["User"] as Employee;
            Employee updatedData = new Employee()
            {
                Id = data.Id,
                Name = data.Name,
                WorkPosition = data.WorkPosition
            };

            if (await _employeeService.Edit(user, updatedData))
                return Ok(true);
            else
                return NotFound();
        }

        [HttpPost("leave")]
        public async Task<bool> LeaveCompany()
        {
            Employee user = HttpContext.Items["User"] as Employee;

            return await _employeeService.LeaveCompany(user.Id);
        }
    }
}
