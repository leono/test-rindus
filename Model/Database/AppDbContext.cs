using Microsoft.EntityFrameworkCore;
using Model.Models;
using System;

namespace Model.Database
{
    public class AppDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().HasData(
                new Employee() { Id = 1, Name = "Admin", DateOfBirth = DateTime.Now, Role = Employee.UserRolesEnum.Admin });

            base.OnModelCreating(modelBuilder);
        }
    }
}
