﻿using Model.Models;
using static Model.Models.Employee;

namespace server.DTOs
{
    public class LoginInfo
    {
        public LoginInfo(Employee user, string token)
        {
            Token = token;
            UserId = user.Id;
            Role = user.Role;
            UserName = user.Name;
            Result = true;
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public UserRolesEnum Role { get; set; }
        public bool Result { get; set; }
        public string Token { get; set; }
    }
}
