using System;
using System.ComponentModel.DataAnnotations;

namespace Model.Models
{
    public class Employee
    {
        public enum UserRolesEnum
        {
            User = 0,
            Admin = 1,
        }

        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string WorkPosition { get; set; }
        public DateTime DateOfBirth { get; set; }
        public UserRolesEnum Role { get; set; }

    }
}
