using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Model.Models;
using Model.Database;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Business.Services
{
    public interface IEmployeeService
    {
        public Task<IEnumerable<Employee>> GetList();
        Task<Employee> GetById(int id);
        Task<bool> NewEmployee(Employee data);
        Task<bool> Edit(Employee user, Employee data);
        Task<bool> LeaveCompany(int id);
    }

    public class EmployeeService : IEmployeeService
    {
        private readonly AppDbContext _dbContext;

        public EmployeeService(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> Edit(Employee user, Employee data)
        {
            var result = false;
            var employee = await _dbContext.Employees.FindAsync(data.Id);
            if (employee != null)
            {
                switch (user.Role)
                {
                    case Employee.UserRolesEnum.User:
                        employee.WorkPosition = data.WorkPosition;
                        break;
                    case Employee.UserRolesEnum.Admin:
                        employee.Name = data.Name;
                        break;
                    default:
                        break;
                }
                _dbContext.Entry(employee).State = EntityState.Modified;
                await _dbContext.SaveChangesAsync();
                result = true;
            }

            return result;
        }

        public async Task<Employee> GetById(int id)
        {
            return await _dbContext.Employees.FindAsync(id);
        }

        public async Task<IEnumerable<Employee>> GetList()
        {
            return await _dbContext.Employees.ToListAsync();
        }

        public async Task<bool> LeaveCompany(int id)
        {
            var employee = await _dbContext.Employees.FindAsync(id);
            _dbContext.Employees.Remove(employee);
            await _dbContext.SaveChangesAsync();
            return true;
        }

        public async Task<bool> NewEmployee(Employee data)
        {
            _dbContext.Employees.Add(data);

            await _dbContext.SaveChangesAsync();
            return true;
        }
    }
}