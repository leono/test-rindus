using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Model.Models;
using Model.Database;
using server.DTOs;
using Business.Helpers;

namespace Business.Services
{
    public interface IUserService
    {
        LoginInfo Authenticate(Employee user);
        Employee GetById(int id);
    }

    public class UserService : IUserService
    {
        private readonly AppSettings _appSettings;
        private readonly AppDbContext _dbContext;

        public UserService(IOptions<AppSettings> appSettings, AppDbContext dbContext)
        {
            _appSettings = appSettings.Value;
            _dbContext = dbContext;
        }

        public LoginInfo Authenticate(Employee user)
        {
            // authentication successful so generate jwt token
            var token = GenerateJwtToken(user);

            return new LoginInfo(user, token);
        }

        public Employee GetById(int id)
        {
            return _dbContext.Employees.FirstOrDefault(x => x.Id == id);
        }

        // helper methods

        private string GenerateJwtToken(Employee user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.JwtSecret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}