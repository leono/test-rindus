using Business.Helpers;
using Business.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Model.Database;
using Model.Models;
using Moq;
using server.Controllers;
using server.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Test
{
    public class UnitTest1 : IDisposable
    {
        ILogger<EmployeeController> logger = null;
        IUserService userService;
        IEmployeeService employeeService;
        IOptions<AppSettings> appSettings = Options.Create(new AppSettings() { JwtSecret = "Test-secret-for-testing" });

        DbContextOptions<AppDbContext> dbOptions = new DbContextOptionsBuilder<AppDbContext>()
            .UseInMemoryDatabase(databaseName: "Test").Options;

        public UnitTest1()
        {
            SetupDatabase();
        }

        private void SetupDatabase(bool clean = false)
        {
            // Insert seed data into the database
            using var context = GetDbTest();
            if (clean)
                context.Employees.RemoveRange(context.Employees.ToList());
            else
            {
                List<Employee> emps = new List<Employee>()
                    {
                        new Employee() { Id = 1, Name = "Admin", Role = Employee.UserRolesEnum.Admin },
                        new Employee() { Id = 2, Name = "User1", Role = Employee.UserRolesEnum.User },
                        new Employee() { Id = 3, Name = "User2", Role = Employee.UserRolesEnum.User },
                    };
                context.Employees.AddRange(emps);
            }
            context.SaveChanges();
        }

        [Fact]
        public async void Test_GetUserList_ShouldReturnListWithElements()
        {
            using var context = GetDbTest();
            // Arrange
            //var employeeSrv = new Mock<IEmployeeService>();
            //mEmployeeSrv.Setup(srv => srv.GetList()).ReturnsAsync(dbContext.Employees.ToList());
            //var controller = new EmployeeController(logger, dbContext, userService, employeeSrv.Object);

            employeeService = new EmployeeService(context);
            userService = new UserService(appSettings, context);
            var controller = new EmployeeController(logger, userService, employeeService);

            // Act
            var result = await controller.Get();

            // Assert
            Assert.True(result.Count() > 0);
            //Assert.Equal(2, result.Count());

        }

        [Fact]
        public async void Test_GetSingleInstance_ShouldReturnInstance()
        {
            using var context = GetDbTest();
            //Arrange
            employeeService = new EmployeeService(context);
            userService = new UserService(appSettings, context);
            var controller = new EmployeeController(logger, userService, employeeService);
            const int id = 1;

            // Act
            var result = await controller.Get(id);
            var okResult = result as OkObjectResult;

            // assert
            Assert.NotNull(okResult);
            Assert.Equal(200, okResult.StatusCode);
            Assert.Equal(id, (okResult.Value as EmployeeDto).Id);
        }

        [Fact]
        public async void Test_GetSingleInstance_ShouldReturnNotFound()
        {
            using var context = GetDbTest();
            //Arrange
            employeeService = new EmployeeService(context);
            userService = new UserService(appSettings, context);
            var controller = new EmployeeController(logger, userService, employeeService);
            const int id = 999;

            // Act
            var result = await controller.Get(id);
            var iAResult = result as NotFoundResult;

            // assert
            Assert.NotNull(iAResult);
            Assert.Equal(404, iAResult.StatusCode);
        }

        //[Fact]
        //public async void Test_NewEmployee_ShouldBeAdminRole()
        //{
        //    using var context = GetDbTest();
        //    //Arrange
        //    employeeService = new EmployeeService(context);
        //    userService = new UserService(appSettings, context);
        //    var controller = new EmployeeController(logger, userService, employeeService);
        //    EmployeeDto newEmp = new EmployeeDto()
        //    {
        //        Name = "Admin",
        //        DateOfBirth = new DateStructDto(DateTime.Now.AddYears(-19))
        //    };
        //    context.Employees.RemoveRange(context.Employees.ToList());
        //    await context.SaveChangesAsync();

        //    // Act
        //    var result = await controller.Post(newEmp);

        //    // assert
        //    Assert.True(result);
        //    Assert.Equal(Employee.UserRolesEnum.Admin, context.Employees.First().Role);
        //}

        [Fact]
        public async void Test_NewEmployee_ShouldBeUserRole()
        {
            using var context = GetDbTest();
            //Arrange
            employeeService = new EmployeeService(context);
            userService = new UserService(appSettings, context);
            var controller = new EmployeeController(logger, userService, employeeService);
            EmployeeDto newEmp = new EmployeeDto()
            {
                Name = Guid.NewGuid().ToString(),
                DateOfBirth = new DateStructDto(DateTime.Now.AddYears(-19))
            };

            // Act
            var result = await controller.Post(newEmp);

            // assert
            Assert.True(result);
            Assert.Equal(Employee.UserRolesEnum.User,
                (await context.Employees.FirstOrDefaultAsync(u => u.Name == newEmp.Name)).Role);
        }

        [Fact]
        public async void Test_Login_ShouldLoginOK()
        {
            using var context = GetDbTest();
            //Arrange
            employeeService = new EmployeeService(context);
            userService = new UserService(appSettings, context);
            var controller = new EmployeeController(logger, userService, employeeService);

            // Act
            var result = await controller.Login(new LoginDto() { UserId = 1 });
            var iAResult = result as OkObjectResult;

            // assert
            Assert.NotNull(iAResult);
            Assert.Equal(200, iAResult.StatusCode);
            Assert.Equal(1, (iAResult.Value as LoginInfo).UserId);
        }

        [Fact]
        public async void Test_Login_ShouldFail()
        {
            using var context = GetDbTest();
            //Arrange
            employeeService = new EmployeeService(context);
            userService = new UserService(appSettings, context);
            var controller = new EmployeeController(logger, userService, employeeService);

            // Act
            var result = await controller.Login(new LoginDto() { UserId = 999 });
            var iAResult = result as BadRequestObjectResult;

            // assert
            Assert.NotNull(iAResult);
            Assert.Equal(400, iAResult.StatusCode);
        }

        [Fact]
        public async void Test_EditEmployeeByRoleAdmin_ShouldChangeOnlyEmployeeName()
        {
            using var context = GetDbTest();
            //Arrange
            employeeService = new EmployeeService(context);
            userService = new UserService(appSettings, context);
            var controller = new EmployeeController(logger, userService, employeeService);

            //prepare data
            Employee empAdmin = await context.Employees
                .AsNoTracking()
                .FirstOrDefaultAsync(c => c.Role == Employee.UserRolesEnum.Admin);
            EmployeeDto data = new EmployeeDto(empAdmin);
            //change data
            data.Name += "_";
            data.Surname += "_";
            data.WorkPosition += "_";
            data.DateOfBirth = new DateStructDto(data.DateOfBirth.ToDate().AddDays(1));

            //prepare context for loggedin user
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Items["User"] = empAdmin;

            // Act
            var result = await controller.Put(data);
            var iAResult = result as OkObjectResult;

            // assert
            Assert.NotNull(iAResult);
            Assert.Equal(200, iAResult.StatusCode);

            //load again the data
            var employeeMod = await context.Employees
                .AsNoTracking()
                .FirstOrDefaultAsync(c => c.Id == empAdmin.Id);

            Assert.Equal(empAdmin.Surname, employeeMod.Surname);
            Assert.Equal(empAdmin.WorkPosition, employeeMod.WorkPosition);
            Assert.Equal(empAdmin.DateOfBirth, employeeMod.DateOfBirth);
        }

        [Fact]
        public async void Test_EditEmployeeByRoleUser_ShouldChangeOnlyWorkPosition()
        {
            using var context = GetDbTest();
            //Arrange
            employeeService = new EmployeeService(context);
            userService = new UserService(appSettings, context);
            var controller = new EmployeeController(logger, userService, employeeService);

            //prepare data
            Employee empUser = await context.Employees
                .AsNoTracking()
                .FirstOrDefaultAsync(c => c.Role == Employee.UserRolesEnum.User);
            EmployeeDto data = new EmployeeDto(empUser);
            //change data
            data.Name += "_";
            data.Surname += "_";
            data.WorkPosition += "_";
            data.DateOfBirth = new DateStructDto(data.DateOfBirth.ToDate().AddDays(1));

            //prepare context for loggedin user
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Items["User"] = empUser;

            // Act
            var result = await controller.Put(data);
            var iAResult = result as OkObjectResult;

            // assert
            Assert.NotNull(iAResult);
            Assert.Equal(200, iAResult.StatusCode);

            //load again the data
            var employeeMod = await context.Employees
                .AsNoTracking()
                .FirstOrDefaultAsync(c => c.Id == empUser.Id);

            Assert.Equal(empUser.Name, employeeMod.Name);
            Assert.Equal(empUser.Surname, employeeMod.Surname);
            Assert.Equal(empUser.DateOfBirth, employeeMod.DateOfBirth);
        }

        [Fact]
        public async void Test_LeaveCompany_ShouldRemoveUserFromDatabase()
        {
            using var context = GetDbTest();
            //Arrange
            employeeService = new EmployeeService(context);
            userService = new UserService(appSettings, context);
            var controller = new EmployeeController(logger, userService, employeeService);

            //prepare data
            Employee empUser = await context.Employees
                .AsNoTracking()
                .FirstOrDefaultAsync(c => c.Role == Employee.UserRolesEnum.User);

            //prepare context for loggedin user
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Items["User"] = empUser;

            // Act
            var result = await controller.LeaveCompany();

            // assert
            Assert.True(result);

            //load again the data
            empUser = await context.Employees
                .AsNoTracking()
                .FirstOrDefaultAsync(c => c.Id == empUser.Id);

            Assert.Null(empUser);
        }

        private AppDbContext GetDbTest()
        {
            return new AppDbContext(dbOptions);
        }

        private IEnumerable<Employee> GetListFake()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            SetupDatabase(true);
        }
    }
}
